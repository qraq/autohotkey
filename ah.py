#!/usr/bin/env python3.13

### CONFIGURATION
CONFIG = 'main.ini' #default config file
SSFILE = 'main.png' #default screenshot file
### CONFIGURATION END

import sys
from time import sleep, time
import tkinter
import re
import threading
import random
from PIL import Image, ImageTk
from mss import tools
from mss.linux import MSS as mss
from pathlib import Path
from pynput.keyboard import Key, Controller
import configparser
import cv2
import numpy as np
import pprint
from screeninfo import get_monitors

#### INIT CFGS
CONFIG = Path(CONFIG)
SSFILE = Path(SSFILE)
#### INIT CFGS

#### DEFAULT HOTKEYS AND %
DEF_HOTKEYS = {
	'hp_high': '82:f1', #HP high (can be called hp_whatever) - activate at 82% press f1
	'hp_med': '65:f2',  #HP medium - activate at 65%, pressing f2
	'hp_low': '42:f2+f3', #HP low - activates at 42%, pressing f2 and f3 (BOTH)
	'mp_low': '20:f4[1050-1150]f4', #MP low - activates at 20%, pressing f4 waiting random 1050-1150ms pressing f4
	'timepress': '40-120', #Time press in ms (min,max)
	'bothpress': '50-100', #Time distance when 2 button pressed at "one" time (example f2+f3)
	'slp_bef': '10-25', #Before activate sleep (ms)
	'lock_hp': '200', #Lock hp activating for desired time (ms)
	'lock_mp': '500', #Lock mp activating for desired time (ms)
	}
#### DEFAULT HOTKEYS AND %

class GetScrCoords():
	def __init__(self, img, help_text='', parent=None):
		raw_coords = 0
		self.raw_coords = (0,0,0,0)

		self.root = tkinter.Tk()
		self.root.attributes("-topmost", True)
		self.mon_w, self.mon_h, self.mon_x, self.mon_y = get_main_monitor()

		scr_width = self.mon_w + (self.mon_x*2)
		scr_height = self.mon_h + (self.mon_y*2)
		#print('Screen Resolution: {0}x{1}'.format(scr_width, scr_height))

		canvas = tkinter.Canvas(width=scr_width, height=scr_height, bg="white", highlightthickness=0, cursor="tcross")
		canvas.pack(expand=tkinter.YES, fill=tkinter.BOTH)

		#Preparing CV2 image to TK
		b,g,r = cv2.split(img) #BGR to RGB
		img = cv2.merge((r,g,b))
		im = Image.fromarray(img)
		img = ImageTk.PhotoImage(image=im)
		canvas.create_image(scr_width/2, scr_height/2, image=img)
		'''
		img = Image.open(imgname)
		loadimg = ImageTk.PhotoImage(img)
		'''

		self.txt = canvas.create_text(5+self.mon_x, 5+self.mon_y, text=help_text, anchor=tkinter.NW, font='Times 20 bold', fill='red')

		canvas.master.overrideredirect(True)
		canvas.master.lift()
		self.zazn = canvas.create_rectangle(0, 0, 0, 0, outline='red')
		canvas.bind('<ButtonPress-1>', self.onStart)
		canvas.bind('<B1-Motion>', self.onGrow)
		canvas.bind('<ButtonRelease-1>', self.onStop)
		self.canvas = canvas
		self.drawn	= None

		self.root.after(30000, lambda: self.root.destroy()) #autokill 30sec
		self.root.mainloop()

	def onStart(self, event):
		self.start = event
		self.drawn = None
	def onGrow(self, event):
		canvas = event.widget
		canvas.coords(self.zazn, self.start.x, self.start.y, event.x, event.y)
		self.endx = event.x
		self.endy = event.y
		self.drawn = self.zazn
	def onStop(self, event):
		if hasattr(self, 'endx') and hasattr(self, 'endy'):
			if max(self.start.x, self.endx) - min(self.start.x, self.endx) > 8 and max(self.start.y, self.endy) - min(self.start.y, self.endy) > 8: #can't be smaller than 8x8px
				self.raw_coords = (
					min(self.start.x, self.endx) - self.mon_x, 
					min(self.start.y, self.endy) - self.mon_y, 
					max(self.start.x, self.endx) - self.mon_x, 
					max(self.start.y, self.endy) - self.mon_y
					) #start x and y always smaller than end
				self.root.destroy()

def get_main_monitor():
	''' Returns list [w, h, x, y] '''
	
	monitors = get_monitors()

	for m in monitors:
		if m.is_primary == True:
			print(m)
			return(m.width, m.height, m.x, m.y)
	return(monitors[0].width, monitors[0].height, monitors[0].x, monitors[0].y)

def get_all_monitors():
	''' Return info about all moniors, sort them from left to right'''

	monitors_info = []

	for m in get_monitors():
		monitors_info.append({
			'w': m.width,
			'h': m.height,
			'x': m.x,
			'y': m.y,
			'primary': m.is_primary,
			'name': m.name
			})
	
	monitors_info = sorted(monitors_info, key=lambda d: d['x'])	#sorting from leftmost (x parameter)

	return monitors_info

def red_mask(hsv):
	lower_red = np.array([0,70,70])
	upper_red = np.array([10,255,255])
	lower_red2 = np.array([176,70,70])
	upper_red2 = np.array([180,255,255])

	red = cv2.inRange(hsv, lower_red, upper_red)
	red2 = cv2.inRange(hsv, lower_red2, upper_red2)

	return red + red2

def blue_mask(hsv):
		lower_blue = np.array([110,70,70])
		upper_blue = np.array([130,255,255])

		return cv2.inRange(hsv, lower_blue, upper_blue)

def crop_selection(img, coords, itype=None):
	start_x, start_y, end_x, end_y = coords #coords should be like [startX(left), startY(top), endX, endY]
	selection = img[start_y:end_y, start_x:end_x] #cropping a selection
	hsv = cv2.cvtColor(selection, cv2.COLOR_BGR2HSV) #convert to HSV

	if itype == 'red_bar':
		mask = red_mask(hsv)

		contours, hierarchy = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)		
		if len(contours) > 0:
			cnt = max(contours, key= cv2.contourArea) #find biggest contour
			x,y,w,h = cv2.boundingRect(cnt)

			crpimg = img[start_y+y:start_y+y+h,start_x+x:start_x+x+w] #cropped img coords
			cropped_y, cropped_x, c = crpimg.shape 
			y_middle = start_y+y + round(cropped_y / 2)
			crpmid = img[y_middle:y_middle+1, start_x+x:start_x+x+w] #1px height coords

			return (start_x+x, y_middle, start_x+x+w, y_middle+1)

		else:
			return False

	elif itype == 'blue_bar':
		mask = blue_mask(hsv)

		contours, hierarchy = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)		
		if len(contours) > 0:
			cnt = max(contours, key= cv2.contourArea) #find biggest contour
			x,y,w,h = cv2.boundingRect(cnt)

			crpimg = img[start_y+y:start_y+y+h,start_x+x:start_x+x+w] #cropped img coords
			cropped_y, cropped_x, c = crpimg.shape 
			y_middle = start_y+y + round(cropped_y / 2)
			crpmid = img[y_middle:y_middle+1, start_x+x:start_x+x+w] #1px height coords

			return (start_x+x, y_middle, start_x+x+w, y_middle+1)
	
	elif itype == 'ref':
		mask = red_mask(hsv)

		contours, hierarchy = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)		
		if len(contours) > 0:
			cnt = max(contours, key= cv2.contourArea) #find biggest contour
			x,y,w,h = cv2.boundingRect(cnt)

			return (start_x+x, start_y+y, start_x+x+w, start_y+y+h)
	
	elif itype == 'cld':
		return (start_x+2, start_y+2, end_x-2, end_y-2) #-2 each side recommended		

def save_ss(filename=SSFILE, delay=3):
	print('Prepare for SS in a {} sec!'.format(delay))
	sleep(delay)
	monitor = {
		"left": ALL_COORDS['x'],
		"top": ALL_COORDS['y'],
		"width": ALL_COORDS['w'],
		"height": ALL_COORDS['h'],
	}
	with mss() as sct:
		img = sct.grab(monitor)
		tools.to_png(img.rgb, img.size, output=str(filename))
	return img

def get_all_coords(imgname=SSFILE):
	img = cv2.imread(str(SS_PATH))
	print(get_main_monitor())
	mon_w, mon_h, mon_x, mon_y = get_main_monitor()
	wres, hres, c = img.shape
	print('Screen res {}x{}'.format(wres, hres))
	conf_dict = {}
	conf_dict['x'] = mon_x
	conf_dict['y'] = mon_y
	conf_dict['w'] = wres
	conf_dict['h'] = hres
	conf_dict['hp'] = GetScrCoords(img, help_text='HP BAR').raw_coords
	conf_dict['mp'] = GetScrCoords(img, help_text='MP BAR').raw_coords
	conf_dict['ref'] = GetScrCoords(img, help_text='Reference point (heart)').raw_coords
	conf_dict['cld'] = GetScrCoords(img, help_text='Heal cooldown').raw_coords

	print(conf_dict['hp'])
	print(conf_dict['mp'])
	print(conf_dict['ref'])
	print(conf_dict['cld'])

	conf_dict['hp'] = crop_selection(img, conf_dict['hp'], 'red_bar')
	conf_dict['mp'] = crop_selection(img, conf_dict['mp'], 'blue_bar')
	conf_dict['ref'] = crop_selection(img, conf_dict['ref'], 'ref')
	conf_dict['cld'] = crop_selection(img, conf_dict['cld'], 'cld')

	return conf_dict

def perform_config(imgname=SSFILE):
	print('Using {}'.format(imgname))
	ALL_COORDS = get_all_coords(imgname=imgname) #Get all needed coords
	print('Coords:')
	for key, value in ALL_COORDS.items():
		print('{} = {}'.format(key, value))
	print('Config NOT saved! Use "sc" to save current coords')
	
	return ALL_COORDS

def display_selections(now=False):
	if now:
		hp_mon = {"top": ALL_COORDS['hp'][1], "left": ALL_COORDS['hp'][0], "width": ALL_COORDS['hp'][2]-ALL_COORDS['hp'][0], "height": ALL_COORDS['hp'][3]-ALL_COORDS['hp'][1]}
		mp_mon = {"top": ALL_COORDS['mp'][1], "left": ALL_COORDS['mp'][0], "width": ALL_COORDS['mp'][2]-ALL_COORDS['mp'][0], "height": ALL_COORDS['mp'][3]-ALL_COORDS['mp'][1]}
		ref_mon = {"top": ALL_COORDS['ref'][1], "left": ALL_COORDS['ref'][0], "width": ALL_COORDS['ref'][2]-ALL_COORDS['ref'][0], "height": ALL_COORDS['ref'][3]-ALL_COORDS['ref'][1]}
		cld_mon = {"top": ALL_COORDS['cld'][1], "left": ALL_COORDS['cld'][0], "width": ALL_COORDS['cld'][2]-ALL_COORDS['cld'][0], "height": ALL_COORDS['cld'][3]-ALL_COORDS['cld'][1]}
		print(ALL_COORDS)
		print(cld_mon)

		##### MONITOR ANLIGMENT FIX
		hp_mon['top'] += ALL_COORDS['y']; hp_mon['left'] += ALL_COORDS['x']
		mp_mon['top'] += ALL_COORDS['y']; mp_mon['left'] += ALL_COORDS['x']
		ref_mon['top'] += ALL_COORDS['y']; ref_mon['left'] += ALL_COORDS['x']
		cld_mon['top'] += ALL_COORDS['y']; cld_mon['left'] += ALL_COORDS['x']
		print(cld_mon)


		with mss() as sct:
			hp_img = sct.grab(hp_mon)
			mp_img = sct.grab(mp_mon)
			ref_img = sct.grab(ref_mon)
			cld_img = sct.grab(cld_mon)

		hp = np.array(hp_img)
		mp = np.array(mp_img)
		ref = np.array(ref_img)
		cld = np.array(cld_img)

		hsv = cv2.cvtColor(hp, cv2.COLOR_BGR2HSV)
		hp_mask = red_mask(hsv)
		hsv = cv2.cvtColor(mp, cv2.COLOR_BGR2HSV)
		mp_mask = blue_mask(hsv)
	else:
		img = cv2.imread(str(SS_PATH))

		hp = img[ALL_COORDS['hp'][1]:ALL_COORDS['hp'][3], ALL_COORDS['hp'][0]:ALL_COORDS['hp'][2]]
		mp = img[ALL_COORDS['mp'][1]:ALL_COORDS['mp'][3], ALL_COORDS['mp'][0]:ALL_COORDS['mp'][2]]
		ref = img[ALL_COORDS['ref'][1]:ALL_COORDS['ref'][3], ALL_COORDS['ref'][0]:ALL_COORDS['ref'][2]]
		cld = img[ALL_COORDS['cld'][1]:ALL_COORDS['cld'][3], ALL_COORDS['cld'][0]:ALL_COORDS['cld'][2]]

		hsv = cv2.cvtColor(hp, cv2.COLOR_BGR2HSV)
		hp_mask = red_mask(hsv)
		hsv = cv2.cvtColor(mp, cv2.COLOR_BGR2HSV)
		mp_mask = blue_mask(hsv)

	cv2.namedWindow('HP', cv2.WINDOW_NORMAL)
	cv2.imshow('HP', hp)

	cv2.namedWindow('MP', cv2.WINDOW_NORMAL)
	cv2.imshow('MP', mp)

	cv2.namedWindow('HP MASK', cv2.WINDOW_NORMAL)
	cv2.imshow('HP MASK', hp_mask)

	cv2.namedWindow('MP MASK', cv2.WINDOW_NORMAL)
	cv2.imshow('MP MASK', mp_mask)

	cv2.namedWindow('REF', cv2.WINDOW_NORMAL)
	cv2.imshow('REF', ref)

	cv2.namedWindow('CLD', cv2.WINDOW_NORMAL)
	cv2.imshow('CLD', cld)

	cv2.waitKey(0)
	cv2.destroyAllWindows()

def bar_test(img, tp='hp'):
	'''Takes img, returns (percent, active_px, full)'''

	img = np.array(img)
	hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
	if tp == 'hp':
		mask = red_mask(hsv)
	else:
		mask = blue_mask(hsv)

	full = mask[0].shape[0] #whole width (100%)
	active = np.count_nonzero(mask) #count 255s
	percent = round(active * 100 / full)

	return (percent, active, full)

def object_exist(img, tp='cld'):
	'''Checks if CLD_IMG exists in img'''

	img = np.array(img)
	gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
	mm = cv2.cvtColor(CLD_IMG, cv2.COLOR_BGR2GRAY)
	
	res = cv2.matchTemplate(gray, mm, cv2.TM_CCOEFF_NORMED)
	threshold = 0.9999 #mniejszy tym zaczyna glupoty lapac 0.1. 0.9999 == ok nie wywala ale ladnie cldwn
	loc = np.where(res >= threshold)

	if loc[0].size > 0:
		l = True
	else:
		l = False

	return l
	

def object_test(img, tp='ref'):
	'''Takes img, returns true/false if obj is its position'''

	img = np.array(img)
	if tp == 'refmask': #little bit more effective (ONLY RED REF POINT)
		hsv = cv2.cvtColor(REF_IMG, cv2.COLOR_BGR2HSV)
		mm = red_mask(hsv) #red mask
		hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
		img = red_mask(hsv)
		match = cv2.matchShapes(img, mm, 2, 0.0)
		if match > 0.0001:
			match = False
		else: 
			match = True

	elif tp == 'ref': #for all colors ref point
		mm = REF_IMG
		img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
		mm = cv2.cvtColor(mm, cv2.COLOR_BGR2GRAY)
		match = cv2.matchShapes(img, mm, 2, 0.0) #0.006108604219232261 - git, wiecej = lipa
		if match > 0.005:
			match = False
		else: 
			match = True
	else:
		mm = CLD_IMG
		img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
		mm = cv2.cvtColor(mm, cv2.COLOR_BGR2GRAY)
		match = cv2.matchShapes(img, mm, 2, 0.0) #0.006108604219232261 - git, wiecej = lipa
		if match > 0.005:
			match = False
		else: 
			match = True
	
	'''
	print(c1)
	cv2.namedWindow('a', cv2.WINDOW_NORMAL)
	cv2.imshow('a', mm)

	cv2.namedWindow('b', cv2.WINDOW_NORMAL)
	cv2.imshow('b', img)

	cv2.waitKey(0)
	cv2.destroyAllWindows()
	'''

	return match

def hot_test(hot_str, test_pct):
	'''Takes hotkey string and %, returns smallest matching one or false'''

	hp_table = [(int(v.split(':')[0]),v.split(':')[1],k) for k,v in HOTKEYS.items() if k.startswith(hot_str)] #example (82, 'f1', 'hp_high')
	a = [a for a in hp_table if a[0] >= test_pct] #return all smaller than cuurent hp
	if a: #if any of them is in list
		return min(a) #return like (82, 'f1', 'hp_high')
	else:
		return False

def keys_press(key_str):
	'''Press keys using str pattern'''
	keyboard = Controller()

	timepress = (int(HOTKEYS['timepress'].split('-')[0]), int(HOTKEYS['timepress'].split('-')[1]))
	bothpress = (int(HOTKEYS['bothpress'].split('-')[0]), int(HOTKEYS['bothpress'].split('-')[1]))

	kpk = re.match(r'^(([A-Za-z\d_])+(\+|\Z)){2,}$', key_str) #Regex to check if match f1+f2 etc
	ktk = re.match(r'^(([A-Za-z\d_])+(\[\d+-\d+\]|\Z)){2,}$', key_str) #Regex to check if match f1[1000]f1 etc

	if kpk:
		hk = ['Key.{}'.format(i) for i in key_str.split('+')]
		for i in hk:
			keyboard.press(eval(i))
			sleep_rand(bothpress[0], bothpress[1])
		sleep_rand(timepress[0], timepress[1]) #wait keypress time
		random.shuffle(hk) #randomize release order
		for i in hk:
			keyboard.release(eval(i))
			sleep_rand(int(bothpress[0]/2), int(bothpress[1]/2))

	elif ktk:
		spl =  re.split(r'(\[\d+-\d+\])',key_str)
		for i in spl:
			if i.startswith('['):
				p = (i[1:-1].split('-'))
				sleep_rand(int(p[0]), int(p[1]))
			else:
				key = 'Key.{}'.format(i)
				keyboard.press(eval(key))
				sleep_rand(timepress[0], timepress[1]) #wait keypress time
				keyboard.release(eval(key))
	else:
		key = 'Key.{}'.format(key_str)
		keyboard.press(eval(key))
		sleep_rand(timepress[0],timepress[1]) #wait keypress time
		keyboard.release(eval(key))

def save_config(config_dict, filename=CONFIG, silent=False):
	config = configparser.ConfigParser()
	save_dict = {}

	for k,v in config_dict.items(): #change tuples to strings LOOP
		if type(v) == tuple:
			save_dict[k] = ','.join(str(i) for i in v)
		else:
			save_dict[k] = v

	config['COORDS'] = save_dict
	config['IMGS'] = {'main': str(SS_PATH)}
	config['HK_DEFAULT'] = DEF_HOTKEYS

	with open(filename, 'w') as configfile:
		config.write(configfile)

	if silent == False:
		print(save_dict)
		print('Coords data saved to {}'.format(filename))

def load_config(config_file=CONFIG, profile='DEFAULT'):
	config = configparser.ConfigParser()
	config.read(config_file)

	global SS_PATH, ALL_COORDS
	SS_PATH = Path(config['IMGS']['main'])

	ALL_COORDS = {}
	for k,v in config['COORDS'].items():
		spl = tuple(int(i) for i in v.split(','))
		if len(spl) > 1:
			ALL_COORDS[k] = spl
		else:
			ALL_COORDS[k] = int(v)
	
	global REF_IMG, CLD_IMG
	if SS_PATH.exists():
		img = cv2.imread(str(SS_PATH))
		REF_IMG = img[ALL_COORDS['ref'][1]:ALL_COORDS['ref'][3], ALL_COORDS['ref'][0]:ALL_COORDS['ref'][2]]
		CLD_IMG = img[ALL_COORDS['cld'][1]:ALL_COORDS['cld'][3], ALL_COORDS['cld'][0]:ALL_COORDS['cld'][2]]
	else:
		print('Error {} doesnt exist!!!'.format(SS_PATH))

	global PROFILE_LST
	PROFILE_LST = [i[3:] for i in config.sections() if i.startswith('HK_')]

	global HK_PROFILE, HOTKEYS, SLP_BEF, HP_LOCK_TIME, MP_LOCK_TIME
	full_name = 'HK_{}'.format(profile.upper())
	if full_name in config.sections():
		HK_PROFILE = full_name
		HOTKEYS = dict(config[HK_PROFILE])

		SLP_BEF = (int(HOTKEYS['slp_bef'].split('-')[0]), int(HOTKEYS['slp_bef'].split('-')[1]))

		HP_LOCK_TIME = int(HOTKEYS['lock_hp']) / 1000
		MP_LOCK_TIME = int(HOTKEYS['lock_mp']) / 1000

		print('Configuration loaded [{}]. SS file [{}]. HK profile: [{}]'.format(config_file, SS_PATH, HK_PROFILE))
		print('Monitor{}, alignment: x:{}, y:{}'.format(ALL_COORDS['monitor'], ALL_COORDS['x'], ALL_COORDS['y']))
		hps = ['{} {}'.format(i[0],i[1]) for i in HOTKEYS.items() if i[0].startswith('hp_')]
		mps = ['{} {}'.format(i[0],i[1]) for i in HOTKEYS.items() if i[0].startswith('mp_')]
		print(' | '.join('{}'.format(i) for i in hps), end=' | ')
		print(' | '.join('{}'.format(i) for i in mps))
		print('Lock times: HP: {}, MP: {}'.format(HP_LOCK_TIME, MP_LOCK_TIME))

		for ac_k, ac_v in ALL_COORDS.items():  #settings overwrite from profile
			if ac_k in HOTKEYS:
				spl = tuple(int(i) for i in HOTKEYS[ac_k].split(','))
				if len(spl) > 1:
					ALL_COORDS[ac_k] = spl
				else:
					ALL_COORDS[ac_k] = int(HOTKEYS[ac_k])
				print('Setting {} overwriten from profile'.format(ac_k))
		
		#print(ALL_COORDS.items()) #Check all coords after profile or no overwrite
	else:
		print('No such profile name "{}"'.format(profile))

def sleep_rand(min, max):
	'''Sleep for random ammount msecs between min, max'''
	sleep(random.randint(min, max) / 1000)

def show_help():
	print(
		'AVAILABLE COMMANDS:\n'
		'q - Quit\n'
		'ss file.png* - SCREEN SHOT and save for further configuation (no file param = default filename)\n'
		'cc file.png* - CONFIG COORDS (no file param = default filename)\n'
		'sc file.ini* - SAVE CONFIGURATION to .ini (no file param = default filename)\n'
		'dc - DISPLAY CONFIG - display all coords, hotkeys, etc\n'
		'ds now*- DISPLAY SELECTIONS shows selections (now - for realtime ss)\n'
		'pp profilename - loads profile or shows available profiles\n'
		't - test all objects, returns allpx and percentage'
		'm - get info about connected monitors'
		)

if __name__ == '__main__':
	print('NewSS | Python {} | (h)elp, (q)uit'.format(sys.version.split()[0]))
	if CONFIG.exists():
		load_config()
	else:
		ALL_COORDS = {}
		ALL_COORDS['m'] = 1
		print('No config found. Monitor 1 selected, use "m" to change.' )

	#MAIN LOOP
	while True:
		command = input(":")

		#QUIT
		if command == 'q':
			exit()

		#HELP
		if command == 'h':
			show_help()

		if command[:1] == 'm':
			monitors = get_all_monitors()

			if command == 'm':
				for i, m in enumerate(monitors, 1):
					print('[M{}] w:{}, h:{}, x:{}, y:{}, primary:{}, name:{}'.format(i, m['w'], m['h'], m['x'], m['y'], m['primary'], m['name']))
				print('Enter monitor nr to set monitor. Example: m1')
			elif command[1:2].isnumeric():
				print('Setting monitor nr {}'.format(command[1:2]))
				ALL_COORDS['monitor'] = command[1:2]
				ALL_COORDS['x'] = monitors[int(command[1:2])-1]['x']
				ALL_COORDS['y'] = monitors[int(command[1:2])-1]['y']
				ALL_COORDS['w'] = monitors[int(command[1:2])-1]['w']
				ALL_COORDS['h'] = monitors[int(command[1:2])-1]['h']
				save_config(ALL_COORDS, CONFIG, True)

		#SCREENSHOT
		if command[:2] == 'ss':
			if re.match(r'ss .+\.png', command):
				filename = re.match(r'ss (.+\.png)', command).group(1)
			else:
				filename = SSFILE
			saved = save_ss(filename)
			print('File saved as {}'.format(saved))

		#COORDS CONFIG
		if command[:2] == 'cc':
			if re.match(r'cc .+\.png', command): #If file.png parameter
				filename = re.match(r'cc (.+\.png)', command).group(1) #get filename
				SS_PATH = Path(filename)
			else:
				SS_PATH = SSFILE #Default main.png file

			if SS_PATH.exists(): #Check filename exist
				ALL_COORDS = perform_config(imgname=SS_PATH) #perform config using provided .png
			else:
				print('{} not exist. Start with "ss" command'.format(SS_PATH))
		
		#DISPLAY SELECTIONS
		if command[:2] == 'ds':
			if command == 'ds now':
				display_selections(True)
			else:
				print('Displaying selections using [{}]. Press ESC to close'.format(SS_PATH))
				if 'ALL_COORDS' in locals():
					display_selections()
				else:
					print('No configuration found. Start with "cc" command to make configuration')

		#SAVE COORDS
		if command[:2] == 'sc':
			if 'ALL_COORDS' in locals():
				if re.match(r'sc .+\.ini', command): #If file.ini
					filename = re.match(r'sc (.+\.ini)', command).group(1) #get filename
					filename = Path(filename)
				else:
					filename = CONFIG #Default main.ini file

				save_config(ALL_COORDS, filename=filename)
			else:
				print('No configuration found. Start with "cc" command to make configuration')
	
		#DISPLAY CONFIG
		if command == 'dc':
			print('ALL COORDS:')
			pprint.pprint(ALL_COORDS, indent=4, compact=True)
			print('Hotkey profile: [{}]:'.format(HK_PROFILE))
			pprint.pprint(HOTKEYS, indent=4)

		if command[:2] == 'pp':
			print('Available profiles {}'.format(PROFILE_LST))
			if command != 'pp':
				load_config(profile=command[3:])

		#TEST ALL OBJECTS
		if command == 't':
			start_time = time() #time measure
			hp_mon = {"top": ALL_COORDS['hp'][1], "left": ALL_COORDS['hp'][0], "width": ALL_COORDS['hp'][2]-ALL_COORDS['hp'][0], "height": ALL_COORDS['hp'][3]-ALL_COORDS['hp'][1]}
			mp_mon = {"top": ALL_COORDS['mp'][1], "left": ALL_COORDS['mp'][0], "width": ALL_COORDS['mp'][2]-ALL_COORDS['mp'][0], "height": ALL_COORDS['mp'][3]-ALL_COORDS['mp'][1]}
			ref_mon = {"top": ALL_COORDS['ref'][1], "left": ALL_COORDS['ref'][0], "width": ALL_COORDS['ref'][2]-ALL_COORDS['ref'][0], "height": ALL_COORDS['ref'][3]-ALL_COORDS['ref'][1]}
			#cld_mon = {"top": ALL_COORDS['cld'][1], "left": ALL_COORDS['cld'][0], "width": ALL_COORDS['cld'][2]-ALL_COORDS['cld'][0], "height": ALL_COORDS['cld'][3]-ALL_COORDS['cld'][1]} #exact position
			cld_mon = {"top": ALL_COORDS['cld'][1]-150, "left": ALL_COORDS['cld'][0], "width": ALL_COORDS['cld'][2]-ALL_COORDS['cld'][0], "height": ALL_COORDS['cld'][3]-ALL_COORDS['cld'][1]+200} #-100px height test

			##### MONITOR ANLIGMENT FIX
			hp_mon['top'] += ALL_COORDS['y']; hp_mon['left'] += ALL_COORDS['x']
			mp_mon['top'] += ALL_COORDS['y']; mp_mon['left'] += ALL_COORDS['x']
			ref_mon['top'] += ALL_COORDS['y']; ref_mon['left'] += ALL_COORDS['x']
			cld_mon['top'] += ALL_COORDS['y']; cld_mon['left'] += ALL_COORDS['x']

			with mss() as sct:
				hp_img = sct.grab(hp_mon)
				mp_img = sct.grab(mp_mon)
				re_img = sct.grab(ref_mon)
				cl_img = sct.grab(cld_mon)
				
			hp = bar_test(hp_img, tp='hp')
			mp = bar_test(mp_img, tp='mp')
			ref = object_test(re_img, tp='refmask')
			#cld = object_test(cl_img, tp='cld') exact object test
			cld = object_exist(cl_img, tp='cld')

			print('HP {}% {}/{}'.format(*hp))
			print('MP {}% {}/{}'.format(*mp))
			print('Ref {}'.format(ref))
			print('Cld {}'.format(cld))

			print('Operation time {:.3f}'.format(time() - start_time))

		#ACTIVATE
		if command == 'a':
			hp_mon = {"top": ALL_COORDS['hp'][1], "left": ALL_COORDS['hp'][0], "width": ALL_COORDS['hp'][2]-ALL_COORDS['hp'][0], "height": ALL_COORDS['hp'][3]-ALL_COORDS['hp'][1]}
			mp_mon = {"top": ALL_COORDS['mp'][1], "left": ALL_COORDS['mp'][0], "width": ALL_COORDS['mp'][2]-ALL_COORDS['mp'][0], "height": ALL_COORDS['mp'][3]-ALL_COORDS['mp'][1]}
			ref_mon = {"top": ALL_COORDS['ref'][1], "left": ALL_COORDS['ref'][0], "width": ALL_COORDS['ref'][2]-ALL_COORDS['ref'][0], "height": ALL_COORDS['ref'][3]-ALL_COORDS['ref'][1]}
			#cld_mon = {"top": ALL_COORDS['cld'][1], "left": ALL_COORDS['cld'][0], "width": ALL_COORDS['cld'][2]-ALL_COORDS['cld'][0], "height": ALL_COORDS['cld'][3]-ALL_COORDS['cld'][1]} #exact position
			cld_mon = {"top": ALL_COORDS['cld'][1]-150, "left": ALL_COORDS['cld'][0], "width": ALL_COORDS['cld'][2]-ALL_COORDS['cld'][0], "height": ALL_COORDS['cld'][3]-ALL_COORDS['cld'][1]+200} #-100px height test
			
			##### MONITOR ANLIGMENT FIX
			hp_mon['top'] += ALL_COORDS['y']; hp_mon['left'] += ALL_COORDS['x']
			mp_mon['top'] += ALL_COORDS['y']; mp_mon['left'] += ALL_COORDS['x']
			ref_mon['top'] += ALL_COORDS['y']; ref_mon['left'] += ALL_COORDS['x']
			cld_mon['top'] += ALL_COORDS['y']; cld_mon['left'] += ALL_COORDS['x']


			hp_lock = mp_lock = 1

			try:
				with mss() as sct:
					while True:
						start_time = time() #time measure
						sleep(0.08)#sleep at start
						re_img = sct.grab(ref_mon)
						cl_img = sct.grab(cld_mon)
						hp_img = sct.grab(hp_mon)
						mp_img = sct.grab(mp_mon)

						hp = bar_test(hp_img, tp='hp')
						mp = bar_test(mp_img, tp='mp')
						ref = object_test(re_img, tp='refmask')
						#cld = object_test(cl_img, tp='cld') exact object test
						cld = object_exist(cl_img, tp='cld')

						if ref:
							hp_test = hot_test('hp_', hp[0])
							mp_test = hot_test('mp_', mp[0])

							if time() > hp_lock + HP_LOCK_TIME: #HP LOCK
								if hp_test:
									if cld:
										sleep_rand(SLP_BEF[0], SLP_BEF[1])
										threading.Thread(target=keys_press, args=(hp_test[1],)).start() #run keypress in new thread
										hp_lock = time()
										print('H{}: {} at {}%'.format(time(),hp_test[2], hp[0]))

							if time() > mp_lock + MP_LOCK_TIME: #MP LOCK
								if mp_test:
									sleep_rand(SLP_BEF[0], SLP_BEF[1])
									threading.Thread(target=keys_press, args=(mp_test[1],)).start() #run keypress in new thread
									mp_lock = time()
									print('M{}: {} at {}%'.format(time(),mp_test[2], mp[0]))

							print('[{}] HP: {[0]}, MP: {[0]}, CLD: {}, FPS: {:.1f}'.format(HK_PROFILE[3:], hp, mp, cld, (1.0 / (time() - start_time))), end='\033[K\r')
						else:
							print('[{}] NO FOCUS, FPS: {:.1f}'.format(HK_PROFILE[3:] ,1.0 / (time() - start_time)), end='\033[K\r')
			
			except KeyboardInterrupt:
				print('Stopped')
				pass
				print(':')